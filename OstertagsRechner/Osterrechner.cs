﻿using System;

namespace OstertagsRechner
{
    public class Osterrechner
    {
        public static int GetOstersonntagTagImMaerz(int jahr)
        {
            int k = jahr / 100;
            int m = 15 + (3*k + 3) / 4 - (8*k + 13) / 25;
            int s = 2 - (3 * k + 3) / 4;
            int a = jahr % 19;
            int d = (19 * a + m) % 30;
            int r = d / 29 + (d / 28 - d / 29) * a / 11;
            int og = 21 + d - r;
            int sz = 7 - ((jahr + jahr / 4 + s) % 7);
            int oe = 7 - ((og - sz) % 7);

            int osterMontag = og + oe;
            return osterMontag;
        }

        public static string GetOstersonntag(int jahr)
        {
            int tagImMaerz = GetOstersonntagTagImMaerz(jahr);
            string osterDatum = "";
            if (tagImMaerz > 31)
            {
                osterDatum = (tagImMaerz - 31) + ". April";
            }
            else
            {
                osterDatum = tagImMaerz + ". März";
            }
            return osterDatum;
        }
    }
}
﻿using System;
using System.Text;

namespace OstertagsRechner
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Bitte gebe das Jahr an (im Format JJJJ, z.B. 1996), für welches das Ostersonntag Datum berechnen weren soll.");
            string inputJahr = Console.ReadLine();
            int jahr = 0;
            if(Int32.TryParse(inputJahr, out jahr) && inputJahr.Length == 4)
            {
                if(jahr > 1581)
                {
                    Console.WriteLine(Osterrechner.GetOstersonntag(jahr));
                }
                else
                {
                    Console.WriteLine("Die angegebene Jahreszahl ist zu niedrig." + System.Environment.NewLine + "Ostersonntag kann mit diesem Programm erst ab dem Jahr 1582 korrekt ermittelt werden");
                }
            }
            else
            {
                Console.WriteLine("Die Eingegebene Zahl ist keine korrekte Jahreszahl!");
            }
            Console.ReadKey();
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;

namespace OstertagsRechner
{
    [TestClass]
    public class OsterrechnerShould
    {
        [TestMethod]
        public void calculateForYear1994_EasterSundayTo38DayOfMarch()
        {
            int jahr = 1996;
            int osterTag = Osterrechner.GetOstersonntagTagImMaerz(jahr);
            // result should be 38
            osterTag.Should().Be(38);
        }

        [TestMethod]
        public void calculateForYear2019_EasterSundayTo52DayOfMarch()
        {
            int jahr = 2019;
            int osterTag = Osterrechner.GetOstersonntagTagImMaerz(jahr);
            osterTag.Should().Be(52);
        }

        [TestMethod]
        public void calculateForYear2019_EasterSundayTo21thApril()
        {
            int jahr = 2019;
            string osterDatum = Osterrechner.GetOstersonntag(jahr);
            osterDatum.Should().Be("21. April");
        }


    }
}
